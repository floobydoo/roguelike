﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Menu : MonoBehaviour
{
    public Canvas MainCanvas;
    public Canvas OptionsCanvas;
    public Canvas SoundCanvas;
    public Canvas Instructions;
    public Canvas Texture;
    public Canvas HUD;

    void Start()
    {
        OptionsCanvas.enabled = false;
        SoundCanvas.enabled = false;
        Instructions.enabled = false;
        Texture.enabled = false;
        bool b = GameController.Instance.NeedsMenu();
        bool m = GameController.Instance.NeedsHUD();
        MainCanvas.enabled = b;
        HUD.enabled = m;
    }

    public void OptionsOn()
    {
        OptionsCanvas.enabled = true;
        MainCanvas.enabled = false;
        Instructions.enabled = false;
        Texture.enabled = false;
    }

    public void ReturnOn()
    {
        OptionsCanvas.enabled = false;
        MainCanvas.enabled = true;
        Instructions.enabled = false;
        Texture.enabled = false;
    }

    public void LoadOn()
    {
        HUD.enabled = true;
        OptionsCanvas.enabled = false;
        MainCanvas.enabled = false;
        Instructions.enabled = false;
        Texture.enabled = false;
    }

    public void SoundOff()
    {
        SoundController.Instance.music.Stop();
        SoundCanvas.enabled = true;
        OptionsCanvas.enabled = false;
    }
    public void SoundOn()
    {
        SoundCanvas.enabled = false;
        OptionsCanvas.enabled = true;
        Instructions.enabled = false;
        Texture.enabled = false;
    }

    public void HowTo()
    {
        SoundCanvas.enabled = false;
        OptionsCanvas.enabled = false;
        Instructions.enabled = true;
        Texture.enabled = false;
    }
    public void ApplySkin()
    {
        SoundCanvas.enabled = false;
        OptionsCanvas.enabled = false;
        Instructions.enabled = false;
        Texture.enabled = true;
    }
}
